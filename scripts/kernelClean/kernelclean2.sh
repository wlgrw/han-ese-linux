( \
KERNEL_HIGHEST=$(dpkg -l 'linux-image-[0-9.]*-[0-9]*-[a-zA-Z0-9]*' | grep ^ii | awk '{print $2}' | sort -V | tail -n 1 | sed 's/^linux-image-\([0-9.]*-[0-9]*\)-.*$/\1/') ; \
KERNEL_CURRENT=$(uname -r | sed "s/\(.*\)-\([^0-9]\+\)/\1/") ; \
sudo apt-get purge $(dpkg -l 'linux-*-[0-9-]*' | grep ^ii | awk '{print $2}' | sed "/$KERNEL_CURRENT/d;/$KERNEL_HIGHEST/d" ) ; \
)